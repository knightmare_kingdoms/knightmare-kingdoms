
#include "stdafx.h"

typedef enum 
{ACAST_BIND,
ACAST_BLIND,
ACAST_DISEASE,
ACAST_CURSE,
ACAST_AGE, 
ACAST_STONE,
ACAST_PARALIZE, 
ACAST_POIZON, 
ACAST_ACID, 
ACAST_DEFAULT}
AFTERCAST_ABILITY;

char aftercast_abilities_table[MONSTERS_AMOUNT];

//=============================================
typedef enum
{ATT_VAMPIRE,
ATT_THUNDER,
ATT_DEATHSTARE,
ATT_DISPEL,
ATT_DISRUPT,
ATT_DEFAULT}
ATTACK_ABILITY;

char attack_abilities_table[MONSTERS_AMOUNT];
//=============================================
typedef enum
{RESIST_DWARF20,
 RESIST_DWARF40,
 RESIST_123LVL,
 RESIST_1234LVL,
 RESIST_MAGICIMMUNE,
 RESIST_ASAIR,
 RESIST_ASEARTH,
 RESIST_ASFIRE,
 RESIST_DEFAULT}
MAGIC_RESISTANCE;

char magic_resistance_table[MONSTERS_AMOUNT];
//=============================================
char missiles_table[MONSTERS_AMOUNT];
//=============================================

char spell_1_table[MONSTERS_AMOUNT];
char spell_2_table[MONSTERS_AMOUNT];
char spell_3_table[MONSTERS_AMOUNT];
//=============================================

int skeltrans[MONSTERS_AMOUNT];
//=============================================

int upgtable[MONSTERS_AMOUNT];

char special_missiles_table[MONSTERS_AMOUNT];
float fire_shield_table[MONSTERS_AMOUNT];
float respawn_table[MONSTERS_AMOUNT];
/*char mana_decrease[MONSTERS_AMOUNT];
char mana_increase[MONSTERS_AMOUNT];

char no_wall_table[MONSTERS_AMOUNT];
char no_range_table[MONSTERS_AMOUNT];


char mana_channel_table[MONSTERS_AMOUNT];
char mana_leak_table[MONSTERS_AMOUNT];

char enchanted[MONSTERS_AMOUNT];


char resource_type_table[MONSTERS_AMOUNT];
char resource_amount_table[MONSTERS_AMOUNT];

char block_table[MONSTERS_AMOUNT];

char double_strike[MONSTERS_AMOUNT];*/


//=============================================




//===================================

extern MONSTER_PROP new_monsters[MONSTERS_AMOUNT];
void LoadCreatureConfig(int target)
{
		char *buf, *c, fname[256];
        FILE *fdesc;

		sprintf(fname,"Data\\creatures\\%u.cfg",target);
        if(fdesc=fopen(fname,"r"))
        {
            //----------
            fseek (fdesc , 0 , SEEK_END);
            int fdesc_size=ftell(fdesc);
            rewind(fdesc);
            //----------
            buf=(char*)malloc(fdesc_size+1);
            fread(buf,1,fdesc_size,fdesc);
            buf[fdesc_size]=0;
            fclose(fdesc);

			c = strstr(buf,"Level=");
			if(c!=NULL)
				new_monsters[target].level = atoi(c+strlen("Level="));

			c = strstr(buf,"Flags=");
			if(c!=NULL)
				new_monsters[target].flags = atoi(c+strlen("Flags="));

			
			c = strstr(buf,"Town=");
			if(c!=NULL)
				new_monsters[target].town = atoi(c+strlen("Town=" ));
//			
///*
//Singular	Plural	Wood	Mercury	Ore	Sulfur	Crystal	Gems	Gold	Fight Value	AI Value	Growth	Horde Growth	
//Hit Points	Speed	Attack	Defense	Low	High	
//Shots	Spells	Low	High	Ability Text	Attributes */
//
//			c = strstr(buf,"Adv.high=" );
//			if (c!=NULL) new_monsters[target].adv_high=
//						atoi(c+strlen("Adv.high=" ));
//			else new_monsters[target].adv_high = 0;
//
//			c = strstr(buf,"Adv.low=" );
//			if (c!=NULL) new_monsters[target].i_AdvLow=
//						atoi(c+strlen("Adv.low=" ));
//			else new_monsters[target].i_AdvLow = 0;
//
//			c = strstr(buf,"AI value=" );
//			if (c!=NULL) new_monsters[target].i_AiValue=
//						atoi(c+strlen("AI value=" ));
//
//			c = strstr(buf,"Attack=" );
//			if (c!=NULL) new_monsters[target].i_Attack =
//						atoi(c+strlen("Attack=" ));
//
//			c = strstr(buf,"CostCrystal=" );
//			if (c!=NULL) new_monsters[target].i_CostCrystal=
//						atoi(c+strlen("CostCrystal=" ));
//
//			c = strstr(buf,"CostGems=" );
//			if (c!=NULL) new_monsters[target].i_CostGems =
//						atoi(c+strlen("CostGems=" ));
//
//			c = strstr(buf,"CostGold=" );
//			if (c!=NULL) new_monsters[target].i_CostGold=
//						atoi(c+strlen("CostGold=" ));
//
//			c = strstr(buf,"CostMercury=" );
//			if (c!=NULL) new_monsters[target].i_CostMercury=
//						atoi(c+strlen("CostMercury=" ));
//
//			c = strstr(buf,"CostOre=" );
//			if (c!=NULL) new_monsters[target].i_CostOre=
//						atoi(c+strlen("CostOre=" ));
//
//			c = strstr(buf,"CostSulfur=" );
//			if (c!=NULL) new_monsters[target].i_CostSulfur=
//						atoi(c+strlen("CostSulfur=" ));
//
//			c = strstr(buf,"CostWood=" );
//			if (c!=NULL) new_monsters[target].i_CostWood=
//						atoi(c+strlen("CostWood=" ));
//
//			c = strstr(buf,"DamageHigh=" );
//			if (c!=NULL) new_monsters[target].i_DamageHigh =
//						atoi(c+strlen("DamageHigh=" ));
//
//			c = strstr(buf,"DamageLow=" );
//			if (c!=NULL) new_monsters[target].i_DamageLow=
//						atoi(c+strlen("DamageLow=" ));
//
//			c = strstr(buf,"Defence=" );
//			if (c!=NULL) new_monsters[target].i_Defence=
//						atoi(c+strlen("Defence=" ));
//
//			c = strstr(buf,"FightValue=" );
//			if (c!=NULL) new_monsters[target].i_FightValue=
//						atoi(c+strlen("Attack=" ));
//
//			c = strstr(buf,"Growth=" );
//			if (c!=NULL) new_monsters[target].i_Growth=
//						atoi(c+strlen("Growth=" ));
//
//			c = strstr(buf,"HP=" );
//			if (c!=NULL) new_monsters[target].i_HitPoints=
//						atoi(c+strlen("HP=" ));
//
//			c = strstr(buf,"Attack=" );
//			if (c!=NULL) new_monsters[target].i_HordeGrowth=
//						atoi(c+strlen("Attack=" ));
//
//			c = strstr(buf,"Shots=" );
//			if (c!=NULL) new_monsters[target].shots=
//						atoi(c+strlen("Shots=" ));
//
//			c = strstr(buf,"Speed=" );
//			if (c!=NULL) new_monsters[target].speed=
//						atoi(c+strlen("Speed=" ));
//
//			c = strstr(buf,"Spells=" );
//			if (c!=NULL) new_monsters[target].spells=
//						atoi(c+strlen("Spells=" ));
//
///*Time between fidgets	Walk Animation Time	Attack Animation Time	
//Flight Animation Distance	Upper-right Missile Offset		
//Right Missile Offset		Lower-right Missile Offset		Missile Frame Angles												
//Troop Count Location Offset	Attack Climax Frame*/
//
///*
//			c = strstr(buf,"TBF=" );
//			if (c!=NULL) new_monsters[target].i_Spells=
//						atoi(c+strlen("TBF=" ));
//*/


			c = strstr(buf,"Spell effect=" );
			if (c!=NULL) aftercast_abilities_table[target]=
						atoi(c+strlen("Spell effect=" ));

			c = strstr(buf,"Attack effect=" );
			if (c!=NULL) attack_abilities_table[target]=
						atoi(c+strlen("Attack effect=" ));

			c = strstr(buf,"Resistance effect=" );
			if (c!=NULL) magic_resistance_table[target]=
				atoi(c+strlen("Resistance effect=" ));

			
			c = strstr(buf,"Spell 1=" );
			if (c!=NULL) spell_1_table[target]=
				atoi(c+strlen("Spell 1=" ));

			
			c = strstr(buf,"Spell 2=" );
			if (c!=NULL) spell_2_table[target]=
				atoi(c+strlen("Spell 2=" ));

			
			c = strstr(buf,"Spell 3=" );
			if (c!=NULL) spell_3_table[target]=
				atoi(c+strlen("Spell 3=" ));
			
			c = strstr(buf,"Sktransformer=" );
			if (c!=NULL) skeltrans[target]=
				atoi(c+strlen("Sktransformer=" ));

			c = strstr(buf,"Shot type=" );
			if (c!=NULL) special_missiles_table[target]=
				atoi(c+strlen("Shot type=" ));

			c = strstr(buf,"Fire shield=" );
			if (c!=NULL) fire_shield_table[target]=
				(float)atof(c+strlen("Fire shield=" ));

			c = strstr(buf,"Self-resurrection=" );
			if (c!=NULL) respawn_table[target]=
				(float)atof(c+strlen("Self-resurrection=" ));


            free(buf);
        }
}

void FillTables()
{
   WIN32_FIND_DATA ffd;
   HANDLE hFind = INVALID_HANDLE_VALUE;

   hFind = FindFirstFile(_T("Data\\creatures\\*.cfg"), &ffd);

   if (INVALID_HANDLE_VALUE == hFind)  {return;} 

   do
   {
      if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
      {
		 int creature = _ttoi(ffd.cFileName);
		 if(creature!=0)
		 {
			LoadCreatureConfig(creature);
		 }
      }
   }
   while (FindNextFile(hFind, &ffd) != 0);
   FindClose(hFind);
}
//=============================================
//fear




//=============================================
//special shots

int __fastcall GetShotType(int creature, int* slot)
{
   return special_missiles_table[creature];
}

__declspec(naked) void f43F72C_Hook()
{
__asm
{
	mov ecx, eax
	mov edx, esi
	call GetShotType
	cmp eax, 1
	je j43F735
	cmp eax, 2
	je j43FB27

	mov eax, 0x43FA31
	jmp eax
	
	j43FB27:
	mov eax, 0x43FB27
	jmp eax

	j43F735:
	mov eax, 0x0043F735
	jmp eax
}
}

__declspec(naked) void f41ED5A_Hook()
{
__asm
{
	push ecx
	push edx
	mov ecx, eax
	mov edx, -1
	call GetShotType
	cmp eax, 0
	jne j41ED69
	mov eax, 0x41ED6D
	pop edx
	pop ecx
	jmp eax
	j41ED69:
	mov eax, 0x41ED69
	pop edx
	pop ecx
	jmp eax

}
}

//============================================
int __stdcall GetFireShield(int creature, int* slot)
{
	char t[256];

sprintf(t,"%i %x %f", creature, slot, respawn_table[creature]);


	//MessageBoxA(0,(LPCSTR)t,(LPCSTR)t,0);
	if (fire_shield_table[creature]>0.01)
	{
		*(int*)0x442E69 = creature*4 + (int)fire_shield_table;
		return 1;
	}
	else
	{
		return 0;
	}
}

__declspec(naked) void f442E61_Hook()
{
__asm
{
	push ecx
	push [ecx+0x34]
	call GetFireShield
	cmp eax, 0
	je j442e6e
	mov eax, 0x442e67
	jmp eax

	j442e6e:
	mov eax, 0x442e6e
	jmp eax

}
}


__declspec(naked) void f4225D6_Hook()
{
__asm
{
	push esi
	push [esi+0x34]
	call GetFireShield
	cmp eax, 0
	je j4225dc
	mov eax, 0x4225e6
	jmp eax
j4225dc:
	mov eax, 0x4225dc
	jmp eax

}
}

//============================================

void CreateAdditionalTables()
{
	//spells
	  memset(spell_1_table,0x09,MONSTERS_AMOUNT);
	  memcpy(spell_1_table+0x0D,(void*)(*(int*)0x44825F),197-0x0D);
	
	  *(char*)0x44824A = 0;
	   *(int*)0x44825F = (int)spell_1_table;
	   
	  memset(spell_2_table,0x08,MONSTERS_AMOUNT);
	  memcpy(spell_2_table+0x0D,(void*)(*(int*)0x447475),197-0x0D);
	
	  *(char*)0x447467 = 0;
	   *(int*)0x447475 = (int)spell_2_table;

	   
	  memset(spell_3_table,0x03,MONSTERS_AMOUNT);
	  memcpy(spell_3_table+0x0D,(void*)(*(int*)0x421479),197-0x0D);
	
	  *(char*)0x42146F = 0;
	   *(int*)0x421479 = (int)spell_3_table;

	//skel
		for (int i=196; i!=MONSTERS_AMOUNT; i++) skeltrans[i]=56;
	    memcpy((char*)skeltrans,(void*)(0x7C3D00),197*4);
	    
	  	*(int*)(0x5664B5+3) = (int)skeltrans;
		*(int*)(0x56689D+3) = (int)skeltrans;
		*(int*)(0x566D79+3) = (int)skeltrans;
		*(int*)(0x566F3C+3) = (int)skeltrans;
		*(int*)(0x566FA4+3) = (int)skeltrans;
		*(int*)(0x566FD1+3) = (int)skeltrans;

		*(int*)(0x5664C9) = MONSTERS_AMOUNT;

	//upgtable

		memset((char*)upgtable, 0xFF, MONSTERS_AMOUNT*sizeof(int));
		
	  	*(int*)(0x724A92+3) = (int)upgtable;
		*(int*)(0x724AA8+3) = (int)upgtable;
		*(int*)(0x74EC7A+3) = (int)upgtable;
		*(int*)(0x74EC94+3) = (int)upgtable;
		*(int*)(0x74ED17+3) = (int)upgtable;
		*(int*)(0x74ED33+3) = (int)upgtable;
		*(int*)(0x75137D+1) = (int)upgtable;
		*(int*)(0x751EC4+1) = (int)upgtable;
		*(int*)(0x752E33+3) = (int)upgtable;
		*(int*)(0x7568F1+3) = (int)upgtable;
		

		*(int*)(0x751378+1) = (int)MONSTERS_AMOUNT*sizeof(int);
		*(int*)(0x751EB9+3) = (int)MONSTERS_AMOUNT*sizeof(int);
	//cast after attack ablilties
	  memset(aftercast_abilities_table,ACAST_DEFAULT,MONSTERS_AMOUNT);
	  memcpy(aftercast_abilities_table+0x16,(void*)(*(int*)0x440237),197-0x16);
	
	  *(char*)0x44022D = 0;
	   *(int*)0x440237 = (int)aftercast_abilities_table;
	  //memset((void*)0x440231,0x90,3);



	//special attacks
	  memset(attack_abilities_table,ATT_DEFAULT,MONSTERS_AMOUNT);
	  memcpy(attack_abilities_table+0x3F,(void*)(*(int*)0x440916),197-0x3F);
	
	  *(char*)0x440908 = 0;
	   *(int*)0x440916 = (int)attack_abilities_table;
	  //memset((void*)0x440909,0x90,3);


    //magic resistance
	  memset(magic_resistance_table,RESIST_DEFAULT,MONSTERS_AMOUNT);
	  memcpy(magic_resistance_table+0x10,(void*)(*(int*)0x44A4B3),197-0x10);
	
	  *(char*)0x44A4A9 = 0;
	   *(int*)0x44A4B3 = (int)magic_resistance_table;

	//shot
		memset(special_missiles_table, 0, MONSTERS_AMOUNT);
		special_missiles_table[CREATURE_MAGOG] = 1;
		special_missiles_table[CREATURE_LICH] = 2;
		special_missiles_table[CREATURE_POWERLICH] = 2;
		special_missiles_table[CREATURE_DRACOLICH] = 2;

		WriteHook((void*)0x41ED5A,(void*)f41ED5A_Hook, HOOKTYPE_JUMP);

		WriteHook((void*)0x43F72C,(void*)f43F72C_Hook,HOOKTYPE_JUMP);

	//fire shield
		for (int i=0; i!=MONSTERS_AMOUNT; i++) fire_shield_table[i]=0;

		fire_shield_table[CREATURE_EFREET_SULTAN] = 0.2;
		//fire_shield_table[CREATURE_CHAMPION] = 1;

		WriteHook((void*)0x4225D6,(void*)f4225D6_Hook,HOOKTYPE_JUMP);
		WriteHook((void*)0x442E61,(void*)f442E61_Hook,HOOKTYPE_JUMP);
	
	//pegasi

	   
	//aftercast_abilities_table[197] = ACAST_ACID;
	//attack_abilities_table[197]=ATT_VAMPIRE;



	FillTables();
}
