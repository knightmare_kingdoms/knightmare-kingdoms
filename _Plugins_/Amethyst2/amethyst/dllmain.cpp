// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.


#include "stdafx.h"


extern void CreateNewTable(void);
extern void CreateAdditionalTables(void);
extern void NewMonstersMissiles(void);
extern void fix_amethyst_crexpmod(void);
//extern void InitNecromancy(void);

int all_creatures;
int conf_creatures;

extern char* experience_modifier_table;

void InitMainConfig(char *config_name)
{
	conf_creatures = 0x34; all_creatures = conf_creatures + 144;
	//MessageBoxA(0, config_name, "Detected Config: ",  0);
    char *buf;

        FILE *fdesc;
        if(fdesc=fopen(config_name,"r"))
        {
            //----------
            fseek (fdesc , 0 , SEEK_END);
            int fdesc_size=ftell(fdesc);
            rewind(fdesc);
            //----------
            buf=(char*)malloc(fdesc_size+1);
            fread(buf,1,fdesc_size,fdesc);
            buf[fdesc_size]=0;
            fclose(fdesc);
            //-----------
            char *c=strstr(buf,"Creatures=");
            if(c==0) {conf_creatures=0x34;}
            else     {conf_creatures=atoi(c+strlen("Creatures="))-144; }
            free(buf);
        }
        else
        {
			conf_creatures = 0x34;
            MessageBoxA(0,config_name, "Missed file: ",0);
        }    
		all_creatures = conf_creatures + 144;
}


// ThisIsNewGame 0x007caf20

/* __declspec(naked) */ void Amethyst_ResetExpTables()
{
	//reset experience tables
	
	//return;
	void(*CrExpSet__Clear)(void) = reinterpret_cast<void (*)(void)> (0x007186C1);
	int(*CrExpMod__Clear)(void) = reinterpret_cast<int (*)(void)> (0x0071A69E);
	int(*CrExpBon__Clear)(void) = reinterpret_cast<int (*)(void)> (0x0071AD24);

	CrExpMod__Clear();

}

void Amethyst_LoadExpTables()
{
	//reset experience tables

	//return;
	int(*CrExpSet__Load)(int) = reinterpret_cast<int(*)(int)> (0x007187ba);
	int(*CrExpMod__Load)(int) = reinterpret_cast<int(*)(int)> (0x0071A58C);
	int(*CrExpBon__Load)(int) = reinterpret_cast<int(*)(int)> (0x0071AAB5);

	//CrExpMod__Load(255); //any argument

}

void Amethyst_SaveExpTables()
{
	//reset experience tables

	//return;
	int(*CrExpSet__Save)(void) = reinterpret_cast<int(*)(void)> (0x00718837);
	int(*CrExpMod__Save)(void) = reinterpret_cast<int(*)(void)> (0x0071A638);
	int(*CrExpBon__Save)(void) = reinterpret_cast<int(*)(void)> (0x0071AB61);

	//CrExpMod__Load(); //any argument

}

// CrExpMod__Dummy        0x00791E80
// CrExpoSet__StopAllBF() 0x0072774E

// void __stdcall CopyExpModTables(TEvent* Event)
/*
__declspec(naked) void CopyExpModTables()
{
	__asm {
		
	}
	// memcpy((void*)experience_modifier_table, (void*)0x0085EB50, 20 * (197 + 5)); // not copies
}
*/

__declspec(naked) void Hook_0x00705450()
{
	__asm {
		pusha
		mov eax, 0x007186C1
		CALL eax
		mov eax, 0x0071A69E
		CALL eax
		mov eax, 0x0071AD24
		CALL eax
		popa

		pop edi
		pop esi
		pop ebx
		mov esp, ebp
		pop ebp
		retn
	}
	// memcpy((void*)experience_modifier_table, (void*)0x0085EB50, 20 * (197 + 5)); // not copies
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{	
			//ConnectEra(); //added by majaczek

		
			/*if (FileExists("Mods\\Amethyst Upgrades\\Data\\amethyst.cfg"))
				InitMainConfig("Mods\\Amethyst Upgrades\\Data\\amethyst.cfg");
			else */ if (FileExists("Mods\\Knightmare Kingdoms\\Data\\amethyst.cfg"))
				InitMainConfig("Mods\\Knightmare Kingdoms\\Data\\amethyst.cfg"); 
			else InitMainConfig("Data\\amethyst.cfg");
			CreateNewTable();
			CreateAdditionalTables();
			fix_amethyst_crexpmod();
			NewMonstersMissiles();
			
			// ConnectEra(); //added by majaczek
			/*
			RegisterHandler(Amethyst_ResetExpTables, "OnAfterErmInstructions");
			RegisterHandler(Amethyst_ResetExpTables, "OnAfterLoadGame");
			*/

			// RegisterHandler(CopyExpModTables, "OnAfterErmInstructions");
			/////WriteHook((void*) /*0x00705347*/ /*0x004cd5d0*/ 0x00705450, (void*)CopyExpModTables, HOOKTYPE_CALL); // at end of CalledBeforeTurn1New
			
			//WriteHook((void*)0x00705450, (void*)Hook_0x00705450, HOOKTYPE_JUMP); // at end of CalledBeforeTurn1New

			// Amethyst_ResetExpTables(); // Unknown (probably doesn't work here)
			
			//Amethyst_LoadExpTables(); // IDK why it doesn't work

			break;
		}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

