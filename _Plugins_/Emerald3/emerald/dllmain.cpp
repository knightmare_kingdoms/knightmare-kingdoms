// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.
#include "emerald.h"

extern void __stdcall  Emerald(PEvent e);
//extern void __stdcall  UndoEmerald(PEvent e);

extern void __stdcall ReallocProhibitionTables(PEvent e);
extern void __stdcall LoadConfigs(PEvent e);
extern void __stdcall blank(int first);


 GAMEDATA save;
 GAMEDATA2 no_save;
Patcher * globalPatcher;
PatcherInstance *emerald;

unsigned long SaveLocker;

extern void DebugWindow(void);
extern void __stdcall DebugWindow(PEvent e);
extern void __stdcall UndoProhibitionTables(PEvent e);

extern void __stdcall EmeraldUndo(PEvent e);
extern void __stdcall EmeraldMove(PEvent e);
extern void __stdcall EmeraldStrings(PEvent e);

unsigned int InChat, InDialog, IsTidy, SpecialDialog, click_locked;

/*
void __stdcall CheckDialog(PEvent e) {
	return;

	static TGameState state;
	*TGetGameState(&state);

	if ((state.CurrentDlgId != state.RootDlgId) ||
		(state.CurrentDlgId != 4205280))
	{
		/// *(ErmY + 1) = temp;
		InDialog = false;
		// Tidy(e);

	}
	else {
		InDialog = true;
		if (state.RootDlgId == 4205280)
			SpecialDialog = true;
		else SpecialDialog = false;
	}
}
*/

/*
void __stdcall Chat(PEvent e) {
	if (SaveLocker) return;

	int tmp = *(ErmY + 1);
	ExecErmCmd("SN:X?y1;");
	if ( *(ErmY + 1) == 0)ExecErmCmd("SN:W^InChat^/1;");
	if ( *(ErmY + 1) == 2)ExecErmCmd("SN:W^InChat^/0;");
	ExecErmCmd("SN:W^InChat^/?y1;");
		
	InChat= *(ErmY + 1);
	 *(ErmY + 1)=tmp;
}
*/

/*
void __stdcall Refresh(PEvent e) {
	//CheckDialog(e);
	//if (InDialog) return;
	
	//emerald->WriteDword(0x7324BD, NEW_ARTS_AMOUNT); //UN:A
	//blank(NON_BLANK_ARTS_AMOUNT);
	
	int UN_A = *(int*) 0x7324BD;
	if (UN_A != NEW_ARTS_AMOUNT )
		IsTidy=false;
	//else IsTidy = true; // _
}
*/


void __stdcall Tidy(PEvent e) {
	//CheckDialog(e);
	//if (InDialog) return;
	
	//if (click_locked) return;
	//Refresh(e);
	//if (IsTidy) return;
	//blank(NON_BLANK_ARTS_AMOUNT);
	Emerald(e);

	ReallocProhibitionTables(e);
	//blank(NON_BLANK_ARTS_AMOUNT);
	//IsTidy = true;
}



void __stdcall Clean(PEvent e) {
	//CheckDialog(e);
	//if (InDialog) return;
	
	//Refresh(e);
	//if (!IsTidy) return;
	UndoProhibitionTables(e);
	EmeraldUndo(e);
	//IsTidy = false;
}


/*
void __stdcall Click(PEvent e) {
	if (SaveLocker) return;

	if (click_locked) {
		//click_locked = false;
		ExecErmCmd("CM:R0;");
		//return;
	}

	//CheckDialog(e);
	//if(InDialog) return;

	int temp = *(ErmY + 1);

	ExecErmCmd("CM:I?y1;");
	//if (*(ErmY + 1) == 3) DebugWindow();

	if (*(ErmY + 1) != 10 ) {
		*(ErmY + 1) = temp;
		if ( (InDialog || click_locked) 
		// && (!save_state)
		) {
			click_locked = false;
			return;
		}

		Tidy(e);
		return;
	}else{

	*(ErmY + 1) = temp;
	Clean(e);
	return;
	}
}
*/

/*
void __stdcall Key(PEvent e) {
	if (SaveLocker) return;

	if (InChat) return;

	CheckDialog(e);
	if (InDialog) return;

	int temp = *(ErmY + 1);
	ExecErmCmd("SN:X?y1;");

	if ( *(ErmY + 1) != 76 && *(ErmY + 1) != 108 &&
		 *(ErmY + 1) != 83 && *(ErmY + 1) != 115 &&
		 *(ErmY + 1) != 69 && *(ErmY + 1) != 101 &&
		true)
	{

		*(ErmY + 1) = temp;
		Tidy(e);  return;
	}
	else {
		*(ErmY + 1) = temp;
		Clean(e); return;
	}

}
*/

void __stdcall InitData3 (PEvent e)
{

	//EmeraldMove(e);

	//pause();
	//EmeraldMove(e);
	//Emerald(e);
	ReallocProhibitionTables(e);
	LoadConfigs(e);
	EmeraldStrings(e);

	//Refresh_Artifact(e);
	//pause();
}


void __stdcall InitData2(PEvent e)
{
	int y2 = *(ErmY + 2);
	ExecErmCmd("SN:W^Emerald Enabled^/?y2;");
	if (*(ErmY + 2) == 1) return;
	ExecErmCmd("SN:W^Emerald Enabled^/1;");
	ReallocProhibitionTables(e);
	//Emerald(e);
	LoadConfigs(e);
	//EmeraldMove(e);
	EmeraldStrings(e);
		
	*(ErmY + 2) = y2;

}



void __stdcall InitData1(PEvent e)
{
	ReallocProhibitionTables(e);
	LoadConfigs(e);
}



void __stdcall StoreData (PEvent e)
{
	
	
	 WriteSavegameSection(sizeof(save), (void*)&save, PINSTANCE_MAIN);
	//WriteSavegameSection(NEW_ARTS_AMOUNT * 2, (void*)&save, PINSTANCE_MAIN);
	 click_locked = true;
	 //SaveLocker = true;
}


void __stdcall RestoreData (PEvent e)
{
	//ExecErmCmd("SN:W^InChat^/0;");
	ReadSavegameSection(sizeof(save), (void*)&save, PINSTANCE_MAIN);
	//Refresh_Artifact(e);
	//ReadSavegameSection(NEW_ARTS_AMOUNT*2, (void*)&save, PINSTANCE_MAIN);
	
	//IsTidy = 2; // two steps
	click_locked = true;
}

void __stdcall Unlock(PEvent e) {
	SaveLocker = false;
	
	//Refresh(e);
	//IsTidy = false;

	Tidy(e);
	//ExecErmCmd("SN:W^InChat^/0;");

	ExecErmCmd("FU99001:P;");
}

void __stdcall Lock(PEvent e) {

	ExecErmCmd("FU99002:P;");

	SaveLocker = true;
	Clean(e);
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		
		static bool FirstTime = true;
		if(FirstTime)
		{
			//DebugWindow();
			//save.footer = 0;
			IsTidy = true; InChat = false; 
			InDialog = true; SpecialDialog = false;
			click_locked = true; SaveLocker = true;


			//strcpy(save.footer, "here is emerald save.");
			//strcpy(save.header, "here is emerald save.");
			char emerald_save_marker[64] = "here is emerald save.";
			memcpy((void *)save.footer, emerald_save_marker, 64);
			memcpy((void *)save.header, emerald_save_marker, 64);
			//strcpy(save.header, "here is emerald save.");

			//strcpy(no_save.footer, "here is emerald not saved.");
			//strcpy(no_save.header, "here is emerald not saved.");
			char emerald_no_save_marker[64] = "here is emerald not saved.";
			memcpy((void *)no_save.footer, emerald_no_save_marker, 64);
			memcpy((void *)no_save.header, emerald_no_save_marker, 64);
			
			FirstTime = false;
		}
		
		 
		globalPatcher = GetPatcher();
		emerald =  globalPatcher->CreateInstance(PINSTANCE_MAIN);
		ConnectEra();

		//Storing data
		//RegisterHandler(InitData2, "OnErmTimer 1");
		//RegisterHandler(InitData1, "OnBeforeErmInstructions");
		
		RegisterHandler(StoreData, "OnSavegameWrite");
		RegisterHandler(RestoreData, "OnSavegameRead");
		//RegisterHandler(InitData1, "OnAfterCreateWindow");



		
		RegisterHandler(InitData3, "OnAfterCreateWindow");
		RegisterHandler(Emerald, "OnAfterWoG"); //original
		
		//RegisterHandler(Click, "OnAdventureMapLeftMouseClick");
		//RegisterHandler(Key, "OnKeyPressed");
		//RegisterHandler(Chat, "OnChat");
		
		RegisterHandler(Lock, "OnBeforeSaveGame");
		RegisterHandler(Unlock, "OnAfterSaveGame");
		RegisterHandler(Unlock, "OnAfterLoadGame");
		RegisterHandler(Unlock, "OnAfterErmInstructions");
	}
	return TRUE;
}

