Name		Cost															Damage				Adv. Map			
Singular	Plural	Wood	Mercury	Ore	Sulfur	Crystal	Gems	Gold	Fight Value	AI Value	Growth	Horde Growth	Hit Points	Speed	Attack	Defense	Low	High	Shots	Spells	Low	High	Ability Text	"Attributes (Reference only, do not change these values)"
Pikeman	Pikemen	0	0	0	0	0	0	60	100	80	14	0	10	4	4	5	1	3	0	0	20	50	"Immune to Champion charge bonus."	0
Halberdier	Halberdiers	0	0	0	0	0	0	75	115	115	14	0	10	5	6	5	2	3	0	0	20	30	"Immune to Champion charge bonus."	0
Archer	Archers	0	0	0	0	0	0	100	115	126	9	0	10	4	6	3	2	3	12	0	16	30		SHOOTING_ARMY
Marksman	Marksmen	0	0	0	0	0	0	150	115	184	9	0	10	6	6	3	2	3	24	0	16	25	Shoots twice.	const_two_attacks | SHOOTING_ARMY
Griffin	Griffins	0	0	0	0	0	0	200	324	351	7	3	25	6	8	8	3	6	0	0	12	25	Retaliates twice.	DOUBLE_WIDE | FLYING_ARMY
Royal Griffin	Royal Griffins	0	0	0	0	0	0	240	364	448	7	3	25	9	9	9	3	6	0	0	12	20	Unlimited retaliations.	DOUBLE_WIDE|FLYING_ARMY
Swordsman	Swordsmen	0	0	0	0	0	0	300	445	445	4	0	35	5	10	12	6	9	0	0	10	20		0
Crusader	Crusaders	0	0	0	0	0	0	400	588	588	4	0	35	6	12	12	7	10	0	0	10	16	Strikes twice.	const_two_attacks
Monk	Monks	0	0	0	0	0	0	400	485	485	3	0	30	5	12	7	10	12	12	0	8	16		SHOOTING_ARMY
Zealot	Zealots	0	0	0	0	0	0	450	500	750	3	0	30	7	12	10	10	12	24	0	8	12	No melee penalty.	SHOOTING_ARMY | const_no_melee_penalty
Cavalier	Cavaliers	0	0	0	0	0	0	1000	1668	1946	2	0	100	7	15	15	15	25	0	0	5	12	Jousting bonus.	DOUBLE_WIDE | const_jousting
Champion	Champions	0	0	0	0	0	0	1200	1800	2100	2	0	100	9	16	16	20	25	0	0	5	10	Jousting bonus.	DOUBLE_WIDE | const_jousting
Angel	Angels	0	0	0	0	0	1	3000	3585	5019	1	0	200	12	20	20	50	50	0	0	4	10	Hates Devils.	FLYING_ARMY | const_raises_morale | KING_2
Archangel	Archangels	0	0	0	0	0	3	5000	6033	8776	1	0	250	18	30	30	50	50	0	5	3	8	"+1 morale.  Hates Devils.
Resurrects allies."	DOUBLE_WIDE | FLYING_ARMY | const_raises_morale | KING_2
																								
																								
																								
Centaur	Centaurs	0	0	0	0	0	0	70	100	100	14	0	8	6	5	3	2	3	12	0	20	50		DOUBLE_WIDE
Centaur Captain	Centaur Captains	0	0	0	0	0	0	90	115	138	14	0	10	8	6	3	2	3	12	0	20	30		DOUBLE_WIDE
Dwarf	Dwarves	0	0	0	0	0	0	120	194	138	8	4	20	3	6	7	2	4	0	0	16	30	20% magic resistance.	0
Battle Dwarf	Battle Dwarves	0	0	0	0	0	0	150	209	209	8	4	20	5	7	7	2	4	0	0	16	25	40% magic resistance.	0
Wood Elf	Wood Elves	0	0	0	0	0	0	200	195	234	7	0	15	6	9	5	3	5	24	0	12	25		SHOOTING_ARMY
Grand Elf	Grand Elves	0	0	0	0	0	0	225	195	331	7	0	15	7	9	5	3	5	24	0	12	20	Shoots twice.	SHOOTING_ARMY | const_two_attacks
Pegasus	Pegasi	0	0	0	0	0	0	250	407	518	5	0	30	8	9	8	5	9	0	0	10	20	Magic damper.	DOUBLE_WIDE | FLYING_ARMY
Silver Pegasus	Silver Pegasi	0	0	0	0	0	0	275	418	532	5	0	30	12	9	10	5	9	0	0	10	16	Magic damper.	DOUBLE_WIDE | FLYING_ARMY
Dendroid Guard	Dendroid Guards	0	0	0	0	0	0	350	690	517	3	2	55	3	9	12	10	14	0	0	8	16	Binds enemies in place.	0
Dendroid Soldier	Dendroid Soldiers	0	0	0	0	0	0	425	765	803	3	2	65	4	9	12	10	14	0	0	8	12	Binds enemies in place.	0
Unicorn	Unicorns	0	0	0	0	0	0	850	1548	1806	2	0	90	7	15	14	18	22	0	0	5	12	Aura of magic resistance.  Blinding attack.	DOUBLE_WIDE
War Unicorn	War Unicorns	0	0	0	0	0	0	950	1740	2030	2	0	110	9	15	14	18	22	0	0	5	10	Aura of magic resistance. Blinding attack.	DOUBLE_WIDE
Green Dragon	Green Dragons	0	0	0	0	1	0	2400	3654	4872	1	0	180	10	18	18	40	50	0	0	4	10	Immune to spell levels 1-3.	DOUBLE_WIDE | FLYING_ARMY | KING_1
Gold Dragon	Gold Dragons	0	0	0	0	2	0	4000	6220	8613	1	0	250	16	27	27	40	50	0	0	3	8	Immune to spell levels 1-4.	DOUBLE_WIDE | FLYING_ARMY | KING_1
																								
																								
																								
Gremlin	Gremlins	0	0	0	0	0	0	30	55	44	16	0	4	4	3	3	1	2	0	0	20	50		0
Master Gremlin	Master Gremlins	0	0	0	0	0	0	40	55	66	16	0	4	5	4	4	1	2	8	0	20	30	Ranged attacker.	SHOOTING_ARMY
Stone Gargoyle	Stone Gargoyles	0	0	0	0	0	0	130	150	165	9	4	16	6	6	6	2	3	0	0	16	30		FLYING_ARMY
Obsidian Gargoyle	Obsidian Gargoyles	0	0	0	0	0	0	160	155	201	9	4	16	9	7	7	2	3	0	0	16	25		FLYING_ARMY
Stone Golem	Stone Golems	0	0	0	0	0	0	150	339	250	6	0	30	3	7	10	4	5	0	0	12	25	Damage from spells reduced 50%.	0
Iron Golem	Iron Golems	0	0	0	0	0	0	200	412	412	6	0	35	5	9	10	4	5	0	0	12	20	Damage from spells reduced 75%.	0
Mage	Magi	0	0	0	0	0	0	350	418	570	4	0	25	5	11	8	7	9	24	0	10	20	"No melee penalty.
Hero spells cost less."	SHOOTING_ARMY | const_no_melee_penalty
Arch Mage	Arch Magi	0	0	0	0	0	0	450	467	680	4	0	30	7	12	9	7	9	24	0	10	16	"No melee penalty.
Hero spells cost less."	SHOOTING_ARMY | const_no_melee_penalty | const_no_wall_penalty
Genie	Genies	0	0	0	0	0	0	550	680	884	3	0	40	7	12	12	13	16	0	0	8	16	Hates Efreet.	FLYING_ARMY
Master Genie	Master Genies	0	0	0	0	0	0	600	748	942	3	0	40	11	12	12	13	16	0	3	8	12	"Spellcaster: Random benefit.
Hates Efreet."	FLYING_ARMY
Naga	Nagas	0	0	0	0	0	0	1100	2016	2016	2	0	110	5	16	13	20	20	0	0	5	12	No enemy retaliation.	DOUBLE_WIDE
Naga Queen	Naga Queens	0	0	0	0	0	0	1600	2485	2840	2	0	110	7	16	13	30	30	0	0	5	10	No enemy retaliation.	DOUBLE_WIDE
Giant	Giants	0	0	0	0	0	1	2000	3146	3718	1	0	150	7	19	16	40	60	0	0	4	10	Mind spell immunity	KING_3
Titan	Titans	0	0	0	0	0	2	5000	5000	7500	1	0	300	11	24	24	40	60	24	0	3	8	No melee penalty. Mind spell immunity.  Hates Black Dragons.	SHOOTING_ARMY | const_no_melee_penalty | KING_3
                                   																								
																								
																								
Imp	Imps	0	0	0	0	0	0	50	50	50	15	8	4	5	2	3	1	2	0	0	20	50		0
Familiar	Familiars	0	0	0	0	0	0	60	60	60	15	8	4	7	4	4	1	2	0	0	20	30	Magic channel.	0
Gog	Gogs	0	0	0	0	0	0	125	145	159	8	0	13	4	6	4	2	4	12	0	16	30		SHOOTING_ARMY
Magog	Magogs	0	0	0	0	0	0	175	210	240	8	0	13	6	7	4	2	4	24	0	16	25	Fireball attack.	SHOOTING_ARMY
Hell Hound	Hell Hounds	0	0	0	0	0	0	200	275	357	5	3	25	7	10	6	2	7	0	0	12	25		DOUBLE_WIDE | FLYING_ARMY
Cerberus	Cerberi	0	0	0	0	0	0	250	308	392	5	3	25	8	10	8	2	7	0	0	12	20	"3-headed attack.
Enemies cannot retaliate."	DOUBLE_WIDE | FLYING_ARMY
Demon	Demons	0	0	0	0	0	0	250	445	445	4	0	35	5	10	10	7	9	0	0	10	20		0
Horned Demon	Horned Demons	0	0	0	0	0	0	270	480	480	4	0	40	6	10	10	7	9	0	0	10	16		0
Pit Fiend	Pit Fiends	0	0	0	0	0	0	500	765	765	3	0	45	6	13	13	13	17	0	0	8	16		0
Pit Lord	Pit Lords	0	0	0	0	0	0	700	1071	1224	3	0	45	7	13	13	13	17	0	1	8	12	"Summon demons from
a dead ally."	0
Efreet	Efreets	0	0	0	0	0	0	900	1413	1670	2	0	90	9	16	12	16	24	0	0	5	12	Immune to fire.  Hates Genies.	0
Efreet Sultan	Efreet Sultans	0	0	0	0	0	0	1100	1584	1848	2	0	90	13	16	14	16	24	0	0	5	10	Fire shield.  Immune to fire.  Hates Genies.	0
Devil	Devils	0	1	0	0	0	0	2700	3759	5101	1	0	160	11	19	21	30	40	0	0	4	10	-1 enemy luck. No enemy retaliation. Hates Angels.	FLYING_ARMY | KING_2
Arch Devil	Arch Devils	0	2	0	0	0	0	4500	5243	7115	1	0	200	17	26	28	30	40	0	0	3	8	-1 enemy luck. No enemy retaliation. Hates Angels.	FLYING_ARMY | KING_2
																								
																								
																								
Skeleton	Skeletons	0	0	0	0	0	0	60	75	60	12	6	6	4	5	4	1	3	0	0	20	50	Undead.	IS_UNDEAD
Skeleton Warrior	Skeleton Warriors	0	0	0	0	0	0	70	85	85	12	6	6	5	6	6	1	3	0	0	20	30	Undead.	IS_UNDEAD
Walking Dead	Walking Dead	0	0	0	0	0	0	100	140	98	8	0	15	3	5	5	2	3	0	0	16	30	Undead.	IS_UNDEAD
Zombie	Zombies	0	0	0	0	0	0	125	160	128	8	0	20	4	5	5	2	3	0	0	16	25	"Undead.
Disease."	IS_UNDEAD
Wight	Wights	0	0	0	0	0	0	200	231	252	7	0	18	5	7	7	3	5	0	0	12	25	Undead.  Regenerating.	FLYING_ARMY | IS_UNDEAD
Wraith	Wraiths	0	0	0	0	0	0	230	252	315	7	0	18	7	7	7	3	5	0	0	12	20	"Undead.  Regenerating.
Drains enemy mana."	FLYING_ARMY | IS_UNDEAD
Vampire	Vampires	0	0	0	0	0	0	360	518	555	4	0	30	6	10	9	5	8	0	0	10	20	"Undead.
No enemy retaliation."	FLYING_ARMY | IS_UNDEAD
Vampire Lord	Vampire Lords	0	0	0	0	0	0	500	652	783	4	0	40	9	10	10	5	8	0	0	10	16	"Undead.  Drains life.
No enemy retaliation."	FLYING_ARMY | IS_UNDEAD
Lich	Liches	0	0	0	0	0	0	550	742	848	3	0	30	6	13	10	11	13	12	0	8	16	Undead.  Death cloud attack.	SHOOTING_ARMY | IS_UNDEAD
Power Lich	Power Liches	0	0	0	0	0	0	600	889	1079	3	0	40	7	13	10	11	15	24	0	8	12	Undead.  Death cloud attack.	SHOOTING_ARMY | IS_UNDEAD
Black Knight	Black Knights	0	0	0	0	0	0	1200	1753	2087	2	0	120	7	16	16	15	30	0	0	5	12	Undead.  Curses enemies.	DOUBLE_WIDE | IS_UNDEAD
Dread Knight	Dread Knights	0	0	0	0	0	0	1500	2029	2382	2	0	120	9	18	18	15	30	0	0	5	10	"Undead.  Curses enemies.  
Death Blow attack."	DOUBLE_WIDE | IS_UNDEAD
Bone Dragon	Bone Dragons	0	0	0	0	0	0	1800	2420	3388	1	0	150	9	17	15	25	50	0	0	4	10	Undead.  -1 to enemy morale.	DOUBLE_WIDE | IS_UNDEAD | const_lowers_morale | FLYING_ARMY | KING_1
Ghost Dragon	Ghost Dragons	0	1	0	0	0	0	3000	3228	4696	1	0	200	14	19	17	25	50	0	0	3	8	Undead.  -1 to enemy morale.  Attack ages enemies.	DOUBLE_WIDE | IS_UNDEAD | const_lowers_morale | FLYING_ARMY | KING_1
																								
																								
																								
Troglodyte	Troglodytes	0	0	0	0	0	0	50	73	59	14	7	5	4	4	3	1	3	0	0	20	50	Immune to Blinding.	0
Infernal Troglodyte	Infernal Troglodytes	0	0	0	0	0	0	65	84	84	14	7	6	5	5	4	1	3	0	0	20	30	Immune to Blinding.	0
Harpy	Harpies	0	0	0	0	0	0	130	140	154	8	0	14	6	6	5	1	4	0	0	16	30	Strike and return.	FLYING_ARMY
Harpy Hag	Harpy Hags	0	0	0	0	0	0	170	196	238	8	0	14	9	6	6	1	4	0	0	16	25	"Strike and return.
No enemy retaliation."	FLYING_ARMY
Beholder	Beholders	0	0	0	0	0	0	250	240	336	7	0	22	5	9	7	3	5	12	0	12	25	No melee penalty.	SHOOTING_ARMY | const_no_melee_penalty
Evil Eye	Evil Eyes	0	0	0	0	0	0	280	245	367	7	0	22	7	10	8	3	5	24	0	12	20	No melee penalty.	SHOOTING_ARMY | const_no_melee_penalty
Medusa	Medusas	0	0	0	0	0	0	300	379	517	4	0	25	5	9	9	6	8	4	0	10	20	"No melee penalty.
Stone gaze."	DOUBLE_WIDE | SHOOTING_ARMY | const_no_melee_penalty
Medusa Queen	Medusa Queens	0	0	0	0	0	0	330	423	577	4	0	30	6	10	10	6	8	8	0	10	16	"No melee penalty.
Stone gaze."	DOUBLE_WIDE | SHOOTING_ARMY | const_no_melee_penalty
Minotaur	Minotaurs	0	0	0	0	0	0	500	835	835	3	0	50	6	14	12	12	20	0	0	8	16	Good morale.	0
Minotaur King	Minotaur Kings	0	0	0	0	0	0	575	890	1068	3	0	50	8	15	15	12	20	0	0	8	12	Good morale.	0
Manticore	Manticores	0	0	0	0	0	0	850	1215	1547	2	0	80	7	15	13	14	20	0	0	5	12		DOUBLE_WIDE | FLYING_ARMY
Scorpicore	Scorpicores	0	0	0	0	0	0	1050	1248	1589	2	0	80	11	16	14	14	20	0	0	5	10	Paralyzing venom.	DOUBLE_WIDE | FLYING_ARMY
Red Dragon	Red Dragons	0	0	0	1	0	0	2500	3762	4702	1	0	180	11	19	19	40	50	0	0	4	10	Immune to spell levels 1-3.	DOUBLE_WIDE | FLYING_ARMY | KING_1
Black Dragon	Black Dragons	0	0	0	2	0	0	4000	6783	8721	1	0	300	15	25	25	40	50	0	0	3	8	Immune to all spells.	DOUBLE_WIDE | FLYING_ARMY | KING_1
																								
																								
																								
Goblin	Goblins	0	0	0	0	0	0	40	60	60	15	8	5	5	4	2	1	2	0	0	20	50		0
Hobgoblin	Hobgoblins	0	0	0	0	0	0	50	65	78	15	8	5	7	5	3	1	2	0	0	20	30		0
Wolf Rider	Wolf Riders	0	0	0	0	0	0	100	130	130	9	0	10	6	7	5	2	4	0	0	16	30		DOUBLE_WIDE
Wolf Raider	Wolf Raiders	0	0	0	0	0	0	140	174	203	9	0	10	8	8	5	3	4	0	0	16	25	Strikes twice	DOUBLE_WIDE
Orc	Orcs	0	0	0	0	0	0	150	175	192	7	0	15	4	8	4	2	5	12	0	12	25		SHOOTING_ARMY
Orc Chieftain	Orc Chieftains	0	0	0	0	0	0	165	200	240	7	0	20	5	8	4	2	5	24	0	12	20		SHOOTING_ARMY
Ogre	Ogres	0	0	0	0	0	0	300	520	416	4	0	40	4	13	7	6	12	0	0	10	20		0
Ogre Mage	Ogre Magi	0	0	0	0	0	0	400	672	672	4	0	60	5	13	7	6	12	0	3	10	16	Spellcaster: Bloodlust.	0
Roc	Rocs	0	0	0	0	0	0	600	790	1027	3	0	60	7	13	11	11	15	0	0	8	16		FLYING_ARMY | DOUBLE_WIDE
Thunderbird	Thunderbirds	0	0	0	0	0	0	700	869	1106	3	0	60	11	13	11	11	15	0	0	8	12	Lightning strike.	FLYING_ARMY | DOUBLE_WIDE
Cyclops	Cyclopes	0	0	0	0	0	0	750	1055	1266	2	0	70	6	15	12	16	20	16	0	5	12	Attacks siege walls.	SHOOTING_ARMY | CATAPULT
Cyclops King	Cyclops Kings	0	0	0	0	0	0	1100	1110	1443	2	0	70	8	17	13	16	20	24	0	5	10	Attacks siege walls.	SHOOTING_ARMY | CATAPULT
Behemoth	Behemoths	0	0	0	0	0	0	1500	3162	3162	1	0	160	6	17	17	30	50	0	0	4	10	Target enemy's defense is reduced 40%.	DOUBLE_WIDE | KING_1
Ancient Behemoth	Ancient Behemoths	0	0	0	0	1	0	3000	5397	6168	1	0	300	9	19	19	30	50	0	0	3	8	Target enemy's defense is reduced 80%.	DOUBLE_WIDE | KING_1
																								
																								
																								
Gnoll	Gnolls	0	0	0	0	0	0	50	70	56	12	6	6	4	3	5	2	3	0	0	20	50		0
Gnoll Marauder	Gnoll Marauders	0	0	0	0	0	0	70	90	90	12	6	6	5	4	6	2	3	0	0	20	30		0
Lizardman	Lizardmen	0	0	0	0	0	0	110	115	126	9	0	14	4	5	6	2	3	12	0	16	30		SHOOTING_ARMY
Lizard Warrior	Lizard Warriors	0	0	0	0	0	0	140	130	156	9	0	15	5	6	8	2	5	24	0	16	25		SHOOTING_ARMY
Gorgon	Gorgons	0	0	0	0	0	0	525	890	890	3	0	70	5	10	14	12	16	0	0	12	25		DOUBLE_WIDE
Mighty Gorgon	Mighty Gorgons	0	0	0	0	0	0	600	1028	1028	3	0	70	6	11	16	12	16	0	0	12	20	Death stare.	DOUBLE_WIDE
Serpent Fly	Serpent Flies	0	0	0	0	0	0	220	215	268	8	0	20	9	7	9	2	5	0	0	10	20	Dispels beneficial spells.	FLYING_ARMY
Dragon Fly	Dragon Flies	0	0	0	0	0	0	240	250	312	8	0	20	13	8	10	2	5	0	0	10	16	Dispels beneficial spells.  Weakens enemies.	FLYING_ARMY
Basilisk	Basilisks	0	0	0	0	0	0	325	506	552	4	0	35	5	11	11	6	10	0	0	8	16	Petrifying attack.	DOUBLE_WIDE
Greater Basilisk	Greater Basilisks	0	0	0	0	0	0	400	561	714	4	0	40	7	12	12	6	10	0	0	8	12	Petrifying attack.	DOUBLE_WIDE
Wyvern	Wyverns	0	0	0	0	0	0	800	1050	1350	2	0	70	7	14	14	14	18	0	0	5	12		DOUBLE_WIDE | FLYING_ARMY
Wyvern Monarch	Wyvern Monarchs	0	0	0	0	0	0	1100	1181	1518	2	0	70	11	14	14	18	22	0	0	5	10	Poisonous.	DOUBLE_WIDE | FLYING_ARMY
Hydra	Hydras	0	0	0	0	0	0	2200	4120	4120	1	0	175	5	16	18	25	45	0	0	4	10	Attacks all adjacent enemies.  Enemies cannot retaliate.	DOUBLE_WIDE | KING_1
Chaos Hydra	Chaos Hydras	0	0	0	1	0	0	3500	5272	5931	1	0	250	7	18	20	25	45	0	0	3	8	Attacks all adjacent enemies.  Enemies cannot retaliate.	DOUBLE_WIDE | KING_1
																								
																								
																								
Air Elemental	Air Elementals	0	0	0	0	0	0	250	324	356	6	0	25	7	9	9	2	8	0	0	8	12	Lightning and firestorm vulnerability.	0
Earth Elemental	Earth Elementals	0	0	0	0	0	0	400	415	330	4	0	40	4	10	10	4	8	0	0	16	30	Meteor shower vulnerability.	0
Fire Elemental	Fire Elementals	0	0	0	0	0	0	350	345	345	5	0	35	6	10	8	4	6	0	0	16	25	"Immune to fire.
Vulnerable to ice."	0
Water Elemental	Water Elementals	0	0	0	0	0	0	300	315	315	6	0	30	5	8	10	3	7	0	0	12	25	"Immune to ice.
Vulnerable to fire."	0
Gold Golem	Gold Golems	0	0	0	0	0	0	500	600	600	3	0	50	5	11	12	8	10	0	0	10	16	Damage from spells reduced 85%.	0
Diamond Golem	Diamond Golems	0	0	0	0	0	0	750	775	775	2	0	60	5	13	12	10	14	0	0	8	12	Damage from spells reduced 95%.	0
																								
																								
																								
Pixie	Pixies	0	0	0	0	0	0	25	40	55	20	10	3	7	2	2	1	2	0	0	20	50		FLYING_ARMY
Sprite	Sprites	0	0	0	0	0	0	30	70	95	20	10	3	9	2	2	1	3	0	0	20	30	No enemy retaliation.	FLYING_ARMY | const_free_attack
Psychic Elemental	Psychic Elementals	0	0	0	0	0	0	750	1431	1669	2	0	75	7	15	13	10	20	0	0	8	16	Attacks all adjacent enemies w/o retaliation. Mind spell immunity.	IMMUNE_TO_MIND_SPELLS | DOUBLE_WIDE | const_free_attack | MULTI_HEADED
Magic Elemental	Magic Elementals	0	0	0	0	0	0	800	1724	2012	2	0	80	9	15	13	15	25	0	0	8	12	Attacks all adjacent enemies w/o retaliation. Spell immunity.	IMMUNE_TO_MIND_SPELLS | DOUBLE_WIDE | const_free_attack | MULTI_HEADED
NOT USED (1)	NOT USED (1)	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0		
Ice Elemental	Ice Elementals	0	0	0	0	0	0	375	315	380	6	0	30	6	8	10	3	7	24	3	12	20	Mind spell immunity. Ice immunity. Fire vulnerability.	IMMUNE_TO_MIND_SPELLS | SHOOTING_ARMY
NOT USED (2) 	NOT USED (2)	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0		
Magma Elemental	Magma Elementals	0	0	0	0	0	0	500	490	490	4	0	40	6	11	11	6	10	0	3	16	25	Mind spell immunity.  Casts protection from Earth.	IMMUNE_TO_MIND_SPELLS
NOT USED (3)	NOT USED (3)	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0		
Storm Elemental	Storm Elementals	0	0	0	0	0	0	275	324	486	6	0	25	8	9	9	2	8	24	3	6	12	No melee penalty.  Lightning and firestorm vulnerability.	IMMUNE_TO_MIND_SPELLS | SHOOTING_ARMY
NOT USED (4)	NOT USED (4)	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0		
Energy Elemental	Energy Elementals	0	0	0	0	0	0	400	360	470	5	0	35	8	12	8	4	6	0	3	12	25	"Casts Protect from Fire. Mind & fire immunity, cold vulnerability."	IMMUNE_TO_MIND_SPELLS | IMMUNE_TO_FIRE_SPELLS | FLYING_ARMY
Firebird	Firebirds	0	0	0	0	0	0	1500	3248	4547	2	0	150	15	18	18	30	40	0	0	4	10	Breath attack.  Fire spell immunity.	IMMUNE_TO_FIRE_SPELLS | DOUBLE_WIDE | FLYING_ARMY | KING_1 | HAS_EXTENDED_ATTACK
Phoenix	Phoenixes	0	1	0	0	0	0	2000	4929	6721	2	0	200	21	21	18	30	40	0	1	4	10	Breath attack.  Fire spell immunity.  Rebirth.	IMMUNE_TO_FIRE_SPELLS | DOUBLE_WIDE | FLYING_ARMY | KING_1 | HAS_EXTENDED_ATTACK
																								
																								
																								
Azure Dragon	Azure Dragons	0	9	0	0	0	0	30000 	56315	78845	1	0	1000	19	50	50	70	80	0	0	1	3	Fear.	DOUBLE_WIDE | FLYING_ARMY | KING_1
Crystal Dragon	Crystal Dragons	0	0	0	0	5	0	20000 	30260	39338	1	0	800	16	40	40	60	75	0	0	1	3	Crystal generation.	DOUBLE_WIDE | FLYING_ARMY | KING_1
Faerie Dragon	Faerie Dragons	0	0	0	0	0	5	10000	16317	19580	1	0	500	15	20	20	20	30	0	5	1	3	Offensive spell caster.	DOUBLE_WIDE | FLYING_ARMY | KING_1
Rust Dragon	Rust Dragons	0	0	0	7	0	0	15000	24030	26433	1	0	750	17	30	30	50	50	0	0	1	3	Spits acid.	DOUBLE_WIDE | FLYING_ARMY | KING_1
Enchanter	Enchanters	0	0	0	0	0	0	750	805	1210	2	0	30	9	17	12	21	21	32	0	5	12	"No melee penalty.
Group spell casters."	SHOOTING_ARMY | const_no_melee_penalty
Sharpshooter	Sharpshooters	0	0	0	0	0	0	400	415	585	4	0	15	9	12	10	12	15	32	0	5	12	No Range or Barrier penalties.	SHOOTING_ARMY
Halfling	Halflings	0	0	0	0	0	0	40	60	75	15	0	4	5	4	2	1	3	24	0	20	50	Positive luck.	SHOOTING_ARMY
Peasant	Peasants	0	0	0	0	0	0	10	15	15	25	0	1	3	1	1	1	1	0	0	20	50		
Boar Rider	Boar Riders	0	0	0	0	0	0	150	145	145	15	0	15	6	6	5	2	3	0	0	16	30		
Mummy Priest	Mummy Priests	0	0	0	0	0	0	300	270	270	8	0	30	5	7	7	3	5	0	0	12	25	Curse.	
Nomad	Nomads	0	0	0	0	0	0	200	285	345	14	0	30	7	9	8	2	6	0	0	12	25	Sandwalkers.	
Rogue	Rogues	0	0	0	0	0	0	100	135	135	12	0	10	6	8	3	2	4	0	0	16	30	Spying.	
Troll	Trolls	0	0	0	0	0	0	500	1024	1024	4	0	40	7	14	7	10	15	0	0	8	12	Regenerating.	
																								
																								
																								
Catapult	Catapults	0	0	0	0	0	0	1	10	500	0	0	1000	0	10	10	0	0	24	0	0	0	Attacks siege walls.	DOUBLE_WIDE | SHOOTING_ARMY | CATAPULT | SIEGE_WEAPON
Ballista	Ballistas	0	0	0	0	0	0	2500	650	600	0	0	250	0	10	10	2	3	24	0	0	0		DOUBLE_WIDE | SHOOTING_ARMY | SIEGE_WEAPON
First Aid Tent	First Aid Tents	0	0	0	0	0	0	750	10	300	0	0	75	0	0	0	0	0	0	0	0	0	Heals troops.	DOUBLE_WIDE | SIEGE_WEAPON
Ammo Cart	Ammo Carts	0	0	0	0	0	0	1000	5	400	0	0	100	0	0	5	0	0	0	0	0	0	Unlimited ammunition.	SIEGE_WEAPON
Arrow Tower	Arrow Towers	0	0	0	0	0	0	1000	5	400	0	0	100	0	10	5	2	4	99	0	0	0	Unlimited ammunition.	SIEGE_WEAPON
Supreme Archangel	Supreme Archangels	0	0	0	0	0	4	10000	18250	26300	1	0	500	18	40	40	75	75	0	13	2	5	"+1(+2) morale. Resurrects twice.
No fear. No enemy retaliation."	DOUBLE_WIDE | FLYING_ARMY | const_raises_morale | KING_2 | const_free_attack
Diamond Dragon	Diamond Dragons	0	0	0	0	3	0	8000	18750	25500	1	0	500	16	36	36	60	75	0	0	2	5	"Immune to all spells. No fear.
Can blind. +1 Gem daily."	DOUBLE_WIDE | FLYING_ARMY | KING_1 | HAS_EXTENDED_ATTACK
Lord of Thunder	Lords of Thunder	0	0	0	0	0	3	10000	15000	22500	1	0	600	12	32	32	60	75	24	24	2	5	"Shoot twice. No melee penalty.
No fear. Thunderclap. Air shield."	SHOOTING_ARMY | const_no_melee_penalty | KING_3
Hell Baron	Hell Barons	0	3	0	0	0	0	9000	19250	25700	1	0	400	17	35	37	45	60	0	0	2	5	"-1(-2) enemy luck. No fear.
Takes Soul. No enemy retaliation."	FLYING_ARMY | KING_2
Blood Dragon	Blood Dragons	0	2	0	0	0	0	6000	12000	15000	1	0	400	14	25	23	38	75	0	0	2	5	"Undead. -1(-2) enemy morale.
Sucks blood. Ages enemy."	DOUBLE_WIDE | IS_UNDEAD | const_lowers_morale | FLYING_ARMY | KING_1
Darkness Dragon	Darkness Dragons	0	0	0	3	0	0	8000	20000	26000	1	0	600	15	33	33	60	75	0	0	2	5	"Immune to all spells. Fearsome.
Gets back. No fear. Darkness."	DOUBLE_WIDE | FLYING_ARMY | KING_1 | HAS_EXTENDED_ATTACK | const_free_attack
Ghost Behemoth	Ghost Behemoths	0	0	0	0	2	0	6000	16250	18250	1	0	600	11	25	25	45	75	0	0	2	5	"+1(+2) luck. Ignores defence.
Answers twice. Ignores obstacles."	DOUBLE_WIDE | KING_1
Hell Hydra	Hell Hydras	0	0	0	2	0	0	7000	15750	17750	1	0	500	10	25	27	38	68	0	0	2	5	"Acid attack. Can regenerate.
No fear. No enemy retaliation."	DOUBLE_WIDE | KING_1 | MULTI_HEADED | SHOOTING_ARMY | const_free_attack
Sacred Phoenix	Sacred Phoenixes	0	3	0	0	0	0	5000	17750	23000	2	0	400	21	28	28	45	60	0	1	2	5	"Fire spell Immunity. Slayer.
Rebirth always. Fire Shield."	IMMUNE_TO_FIRE_SPELLS | DOUBLE_WIDE | FLYING_ARMY | KING_1 | HAS_EXTENDED_ATTACK
Ghost	Ghosts	0	0	0	0	0	0	500	150	250	7	0	21	8	11	8	3	6	0	0	16	30	"Attract dead souls."	IS_UNDEAD | IS_GHOST | FLYING_ARMY
Emissary of War	Emissaries of War	0	0	0	0	0	0	30000	10	20000	0	0	2000	4	0	10	0	0	0	0	0	0	"Increases Hero's Attack Skill by 1-3 per week"	
Emissary of Peace	Emissaries of Peace	0	0	0	0	0	0	30000	10	20000	0	0	2000	4	0	10	0	0	0	0	0	0	"Increase Hero's Defense Skill by 1-3 per week"	
Emissary of Mana	Emissaries of Mana	0	0	0	0	0	0	30000	10	20000	0	0	2000	4	0	10	0	0	0	0	0	0	"Increase Hero's Spell Power by 1-3 per week"	
Emissary of Lore	Emissaries of Lore	0	0	0	0	0	0	30000	10	20000	0	0	2000	4	0	10	0	0	0	0	0	0	"Increase Hero's Knowledge by 1-3 per week"	
Fire Messenger	Fire Messengers	0	0	0	0	0	0	750	775	775	2	0	70	5	14	12	13	18	0	0	8	10	"Protected against fire spells.
No fear."	
Earth Messenger	Earth Messengers	0	0	0	0	0	0	750	775	775	2	0	70	5	13	14	10	14	0	0	8	10	"Protected against earth spells.
No fear."	
Air Messenger	Air Messengers	0	0	0	0	0	0	750	775	775	2	0	70	6	13	12	10	14	0	0	8	10	"Protected against air spells.
No fear."	
Water Messenger	Water Messengers	0	0	0	0	0	0	750	775	775	2	0	80	5	13	12	10	14	0	0	8	10	"Protected against water spells.
No fear."	
Gorynych	Gorynyches	0	0	0	0	0	0	3500	5272	5931	1	0	250	8	20	20	25	45	0	0	2	5	"Attacks all adjacent enemies.
No fear. No Enemy retaliation."	DOUBLE_WIDE | KING_1 | MULTI_HEADED
War Zealot	War Zealots	0	0	0	0	0	0	600	700	1000	3	0	40	8	14	10	10	12	24	0	6	10	"Protected by Magic Mirror.
No fear. No melee penalty."	SHOOTING_ARMY | const_no_melee_penalty
Arctic Sharpshooter	Arctic Sharpshooters	0	0	0	0	0	0	500	500	700	3	0	15	9	13	10	13	17	32	0	8	12	"No walls or range penalty.
No fear. Air shield."	SHOOTING_ARMY | const_no_wall_penalty
Lava Sharpshooter	Lava Sharpshooters	0	0	0	0	0	0	500	500	700	3	0	15	9	12	11	13	17	32	0	8	12	"No walls or range penalty.
No fear. Fire shield."	SHOOTING_ARMY | const_no_wall_penalty
Nightmare	Nightmares	0	0	0	0	0	0	1500	2050	2400	2	0	110	9	18	16	20	24	0	0	5	10	"Extended Death stare. No fear.
Mind spell immunity."	DOUBLE_WIDE
Santa Gremlin	Santa Gremlins	0	0	0	0	0	0	200	500	200	16	0	5	5	5	5	2	3	0	1000	20	50	"Ice Bolt attack. Can gift
Summon guards."	0
Paladin	Paladins	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Hierophant	Hierophants	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Temple Guardian	Temple Guardians	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Succubus	Succubuses	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Soul Eater	Soul Eaters	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Brute	Brutes	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Ogre Leader	Ogre Leaders	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Shaman	Shamans	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Astral Spirit	Astral Spirits	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Paladin	Paladins	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Hierophant	Hierophants	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Temple Guardian	Temple Guardians	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Succubus	Succubuses	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Soul Eater	Soul Eaters	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Brute	Brutes	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Ogre Leader	Ogre Leaders	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Shaman	Shamans	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Astral Spirit	Astral Spirits	0	0	0	0	0	0	140	130	156	9	0	50	5	5	5	10	10	24	0	16	25		SHOOTING_ARMY
Sylvan Centaur	Sylvan Centaurs	0	0	0	0	0	0	250	250	400	14	0	15	8	10	5	4	6	24	0	10	15	Shoots twice.
Merry.	SHOOTING_ARMY | const_two_attacks
Sorceress	Sorceresses	0	0	0	0	0	0	600	200	1150	2	0	35	8	15	13	10	13	24	0	5	12	Ranged spell caster.	SHOOTING_ARMY
Werewolf	Werewolves	0	0	0	0	0	0	300	460	460	4	0	35	7	10	12	6	9	0	0	10	20	"Lycanthropy (x2 if full moon).
Berserk if full moon: days 14-16"	0
Hell Steed	Hell Steeds	0	0	0	0	0	0	1100	1850	2150	2	0	100	8	15	15	18	24	0	0	5	10	Fire Shield. Fire Wall.
Immune to Fire Magic.	DOUBLE_WIDE
Dracolich	Dracoliches	0	9	0	0	0	0	15000	40000	55000	1	1	800	16	45	45	50	80	5	0	1	3	Undead.  -1 to enemy morale. 20% Magic Resist. 20% Block.	DOUBLE_WIDE
Fanatic	Fanatics	0	0	0	0	0	0	460	600	600	4	0	40	7	13	13	10	13	0	0	10	16	 Attacks twice, magic immune	0
Monstrous Eyes	Monstrous Eyes	0	0	0	0	0	0	310	285	400	7	0	26	7	11	9	3	6	32	0	12	20	Shoots twice. 	SHOOTING_ARMY
Ancient Dendroid	Ancient Dendroids	0	0	0	0	0	0	500	900	1200	3	0	75	6	11	15	12	16	0	0	8	12	Binds enemies.	
Blood Minotaur	Blood Minotaurs	0	0	0	0	0	0	750	990	1268	3	0	55	8	17	15	15	20	0	0	8	12	Positive morale. Bloodloost. Earth Protection. Fearless.	0
Mineral Elemental	Mineral Elementals	0	0	0	0	0	0	640	565	665	4	0	45	6	11	14	8	12	0	5	16	25	"Lighting Immune. Earth Protection. 30% magic resistance." Caster: Earth Spells	IMMUNE_TO_MIND_SPELLS
Electricity Elemental	Electricity Elementals	0	0	0	0	0	0	510	375	600	5	0	40	10	12	10	5	7	0	5	12	25	"Immune to Lighting and Fire. Air Protection." Caster: Multiple Clone.	IMMUNE_TO_MIND_SPELLS | IMMUNE_TO_FIRE_SPELLS | FLYING_ARMY
Ancient Basilisk	Ancient Basilisks	0	0	0	0	0	0	550	661	814	4	0	50	7	13	13	7	10	0	0	8	12	"Better Stonifing.
Water Protection. Fearless."	DOUBLE_WIDE
Acid Wivern	Acid Wiverns	0	0	0	0	0	0	1400	1481	1700	2	0	90	14	15	15	20	23	0	0	5	10	Acid Breath, Stop Counterattack, Poisonous	DOUBLE_WIDE | KING_1 | MULTI_HEADED
Efreet Rajah	Efreet Rajahs	0	0	0	0	0	0	1400	1584	1848	2	0	100	14	18	14	19	24	0	0	5	10	Fire and Air Shield. Fire Immune. Hates Genies.	0
Cesar Medusa	Cesar Medusas	0	0	0	0	0	0	450	500	700	4	0	30	7	11	10	6	9	8	0	10	16	"No Meelee and Range Penalty. Stonifing. Fire Shield."	DOUBLE_WIDE | SHOOTING_ARMY | const_no_melee_penalty
Stormbird	Stormbirds	0	0	0	0	0	0	800	1054	1300	3	0	70	13	14	13	13	17	0	0	8	12	Lighting Strike, Deathstare	DOUBLE_WIDE
Hurricane Elemental	Hurricane Elementals	0	0	0	0	0	0	300	350	399	6	0	28	8	10	10	5	8	24	5	6	12	Shoots twice, Caster: Air Spells	SHOOTING_ARMY
Pit Master	Pit Master	0	0	0	0	0	0	800	1171	1350	3	0	55	9	15	15	14	19	0	5	8	12	Fire Shield. Firewall.
Fire Protection. twohex attack	DOUBLE_WIDE
Ancient Lich	Ancient Liches	0	0	0	0	0	0	800	989	1200	3	0	50	8	15	11	13	17	32	0	8	12	Undead. -1 enemy morale., 20% chance to block 	DOUBLE_WIDE
Gnoll Raider	Gnoll Raiders	0	0	0	0	0	0	100	90	90	12	6	10	7	5	6	3	6	0	0	20	30		0
Greater Demon	Greater Demons	0	0	0	0	0	0	300	480	480	4	0	45	7	12	12	8	10	0	0	10	16		0
Cesar Griffin	Cesar Griffins	0	0	0	0	0	0	275	364	448	7	3	30	11	10	10	4	8	0	0	12	20	Unlimited retaliations.	DOUBLE_WIDE|FLYING_ARMY
Holy Champion	Holy Champions	0	0	0	0	0	0	1500	1800	2100	2	0	150	12	18	18	30	35	0	0	5	10	Jousting bonus.	DOUBLE_WIDE | const_jousting
Dwarf Defender	Dwarf Defenders	0	0	0	0	0	0	175	217	217	8	0	25	5	7	8	2	5	0	0	20	50	Placeholder	Placeholder
Legendary Unicorn	Legendary Unicorns	0	0	0	0	0	0	1200	1840	2130	2	0	150	10	16	16	20	24	0	0	20	50	Placeholder	Placeholder
Cathedral Gargoyle	Cathedral Gargoyles	0	0	0	0	0	0	175	160	201	9	0	20	10	9	9	4	5	0	0	20	50	Placeholder	Placeholder
Skeleton Archer	Skeleton Archers	0	0	0	0	0	0	100	95	95	12	0	10	6	7	7	2	4	10	0	20	50	Placeholder	Placeholder
Ogre Warlock	Ogre Warlocks	0	0	0	0	0	0	500	700	700	4	0	75	5	13	7	6	12	0	5	20	50	Placeholder	Placeholder
Dire Wolf	Dire Wolfs	0	0	0	0	0	0	200	213	213	9	0	15	10	9	6	4	6	0	0	20	50	Placeholder	Placeholder
Arcane Reptilion	Arcane Reptilions	0	0	0	0	0	0	200	144	166	9	0	18	6	8	10	3	6	36	1000	20	50	Placeholder	Placeholder
Blood Hornet	Blood Hornets	0	0	0	0	0	0	300	300	350	8	0	30	14	10	8	4	6	0	0	20	50	Placeholder	Placeholder
Golden Gorgon	Golden Gorgons	0	0	0	0	0	0	650	1151	1151	3	0	70	6	13	13	12	16	0	0	20	50	Placeholder	Placeholder
Life Elemental	Life Elementals	0	0	0	0	0	0	400	324	512	6	0	35	6	10	10	4	8	36	5	20	50	Caster: Ressurection	Placeholder
Void Elemental	Void Elementals	0	0	0	0	0	0	1000	2048	2048	2	0	100	10	15	13	15	25	0	5	20	50	Caster: Summon Elementals	Placeholder
Nosferatu	Nosferatus	0	0	0	0	0	0	600	700	800	4	0	50	10	10	10	5	10	0	0	20	50	Placeholder	Placeholder
Nymph	Nymphs	0	0	0	0	0	0	50	100	120	20	0	5	10	2	2	2	4	0	0	20	50	Placeholder	Placeholder
Musketeer	Musketeers	0	0	0	0	0	0	200	115	184	9	0	10	6	6	3	7	11	12	0	20	50	Placeholder	Placeholder
Hunter	Hunters	0	0	0	0	0	0	250	195	366	7	0	20	8	12	6	6	10	36	0	20	50	Placeholder	Placeholder
Master Archmage	Master Archmages	0	0	0	0	0	0	500	512	720	4	0	35	8	12	10	9	12	36	0	20	50	Placeholder	Placeholder
Sacred Elf	Sacred Elves	0	0	0	0	0	0	300	425	550	5	0	35	12	12	11	6	10	0	1	20	50	Caster: Ressurection	Placeholder
Arcane Genies	Arcane Genies	0	0	0	0	0	0	750	800	1000	3	0	50	12	13	13	14	18	0	5	20	50	Placeholder	Placeholder
Phosforous Troglodite	Phosforous Troglodytes	0	0	0	0	0	0	75	95	95	14	0	7	5	6	6	1	3	0	0	20	50	Placeholder	Placeholder
Seducing Harpy	Seducing Harpies	0	0	0	0	0	0	200	256	256	8	0	18	10	8	7	2	4	0	0	20	50	Placeholder	Placeholder
Grim Reaper	Grim Reapers	0	0	0	0	0	0	1750	2200	2500	2	0	150	10	19	19	18	30	0	0	20	50	Placeholder	Placeholder
Scamp	Scamps	0	0	0	0	0	0	75	70	70	14	0	6	8	5	4	1	2	0	0	20	50	Placeholder	Placeholder
Succubus	Succubusses	0	0	0	0	0	0	200	256	256	8	0	20	6	8	5	2	5	36	0	20	50	Placeholder	Placeholder
Astral Cerberus	Astral Cerberus	0	0	0	0	0	0	275	360	440	7	0	30	9	10	9	2	8	0	0	20	50	Placeholder	Placeholder
Orc Leader	Orc Leaders	0	0	0	0	0	0	200	222	256	7	0	25	5	9	4	3	7	36	0	20	50	Placeholder	Placeholder
Spider Queen	Spider Queens	0	0	0	0	0	0	1700	3000	3600	2	0	120	7	16	13	30	30	0	1000	20	50	Placeholder	Placeholder
Centamoth Thrower	Centamoth Throwers	0	0	0	0	0	0	1400	1280	1600	2	0	80	8	18	14	18	35	36	0	20	50	Placeholder	Placeholder
Female Warlock	Female Warlocks	0	0	0	0	0	0	1250	1300	1600	2	0	100	11	17	17	16	22	1000	5	20	50	Placeholder	Placeholder
Castle Guardian	Castle Guardians	0	0	0	0	0	0	65000	60000	77500	1	1	1000	20	77	77	100	100	0	1000	1	3	Placeholder	Placeholder
Rampart Guardian	Rampart Guardians	0	0	0	0	0	0	65000	60000	77500	1	1	1000	20	77	77	100	100	0	1000	1	3	Placeholder	Placeholder
Tower Guardian	Tower Guardians	0	0	0	0	0	0	65000	60000	77500	1	1	1000	20	77	77	100	100	0	1000	1	3	Placeholder	Placeholder
Inferno Guardian	Inferno Guardians	0	0	0	0	0	0	65000	60000	77500	1	1	1000	20	77	77	100	100	0	1000	1	3	Placeholder	Placeholder
Necro Guardian	Necro Guardians	0	0	0	0	0	0	65000	60000	77500	1	1	1000	20	77	77	100	100	1000	1000	1	3	Placeholder	Placeholder
Dungeon Guardian	Dungeon Guardian	0	0	0	0	0	0	65000	60000	77500	1	1	1000	20	77	77	100	100	0	1000	1	3	Placeholder	Placeholder
Stronghold Guardian	Stronghold Guardians	0	0	0	0	0	0	65000	60000	77500	1	1	1000	20	77	77	100	100	0	1000	1	3	Placeholder	Placeholder
Fortress Guardian	Fortress Guardians	0	0	0	0	0	0	65000	60000	77500	1	1	1000	20	77	77	100	100	0	1000	1	3	Placeholder	Placeholder
Conflux Guardian	Conflux Guardians	0	0	0	0	0	0	65000	60000	77500	1	1	1000	20	77	77	100	100	0	1000	1	3	Placeholder	Placeholder
Red Wizard	Red Wizards	0	0	0	0	0	0	600	560	820	3	0	40	8	13	11	9	12	36	0	20	50	Placeholder	Placeholder
Templar	Templars	0	0	0	0	0	0	500	610	610	4	0	50	7	15	14	10	13	0	1000	10	16	 Attacks twice, magic immune	0
Druid	Druids	0	0	0	0	0	0	600	700	1000	2	0	50	12	17	13	12	16	24	1000	6	10	"No melee penalty."	SHOOTING_ARMY | const_no_melee_penalty
Supreme Dracolich	Supreme Dracoliches	0	9	0	0	0	0	15000	50000	68750	1	1	1000	20	57	57	64	100	5	1000	1	3	Undead.  -1 to enemy morale. 25% Block.	DOUBLE_WIDE
Anubis Knight	Anubis Knights	0	0	0	0	0	0	1750	2200	2500	2	0	150	10	19	19	18	30	0	0	20	50	Placeholder	Placeholder
Elephant Balista	Elephant Balistas	0	0	0	0	0	0	500	500	700	3	0	250	9	12	11	13	17	32	0	8	12	"No walls or range penalty.
No fear. Fire shield."	SHOOTING_ARMY | const_no_wall_penalty
Rainbow Snake	Rainbow Snakes	0	0	0	0	0	0	175	200	200	8	0	25	5	6	6	3	4	0	0	12	25	 	 
Young Pirate	Young Pirates	0	0	0	0	0	0	340	300	500	7	0	30	7	12	10	4	8	32	0	12	20	 	 
Wererat	Wererats	0	0	0	0	0	0	340	300	500	7	0	30	7	12	10	4	8	32	0	12	20	 	 
Rat	Rats	0	0	0	0	0	0	175	200	200	8	0	25	5	6	6	3	4	0	0	12	25	 	 
Demon Prince	Demon Princes	0	0	0	0	0	0	300	480	480	4	0	45	7	12	12	8	10	0	0	10	16		0
Harpy Countess	Harpy Countesses	0	0	0	0	0	0	250	260	260	8	0	18	10	8	7	4	6	0	0	20	50	Placeholder	Placeholder
Darkness Acoilte	Darkness Acolytes	0	0	0	0	0	0	750	805	1210	2	0	30	9	17	12	21	21	32	1000	5	12	"No melee penalty.
Group spell casters."	SHOOTING_ARMY | const_no_melee_penalty
Ice Acolite	Ice Acolytes	0	0	0	0	0	0	750	805	1210	2	0	30	9	17	12	21	21	32	1000	5	12	"No melee penalty.
Group spell casters."	SHOOTING_ARMY | const_no_melee_penalty
Fire Acolite	Fire Acolytes	0	0	0	0	0	0	750	805	1210	2	0	30	9	17	12	21	21	32	1000	5	12	"No melee penalty.
Group spell casters."	SHOOTING_ARMY | const_no_melee_penalty
War Priest	War Priests	0	0	0	0	0	0	750	805	1210	2	0	30	9	17	12	21	21	0	1000	5	12	"No melee penalty.
Group spell casters."	SHOOTING_ARMY | const_no_melee_penalty
Raido	Raido	0	0	0	0	0	0	750	805	1210	2	0	30	9	17	12	21	21	32	1000	5	12	"No melee penalty.
Group spell casters."	SHOOTING_ARMY | const_no_melee_penalty
Coulubrine	Coulubrines	0	0	0	0	0	0	500	500	700	3	0	750	9	12	11	13	17	32	0	8	12	"No walls or range penalty.
No fear. Fire shield."	SHOOTING_ARMY | const_no_wall_penalty
Mantis	Mantis	0	0	0	0	2	0	6000	16250	18250	1	0	600	11	25	25	45	75	0	0	2	5	"+1(+2) luck. Ignores defence.
Answers twice. Ignores obstacles."	DOUBLE_WIDE | KING_1
Ghost behemoth stats?
Magma Dragon	Magma Dragons	0	0	0	3	0	0	8000	20000	26000	1	0	600	15	33	33	60	75	0	0	2	5	"Immune to all spells. Fearsome.
Gets back. No fear. Darkness."	DOUBLE_WIDE | FLYING_ARMY | KING_1 | HAS_EXTENDED_ATTACK | const_free_attack
Sylvan Dragon	Sylvan Dragons	0	0	0	0	3	0	8000	18750	25500	1	0	500	16	36	36	60	75	0	0	2	5	"Immune to all spells. No fear.
Can blind. +1 Gem daily."	DOUBLE_WIDE | FLYING_ARMY | KING_1 | HAS_EXTENDED_ATTACK
Astral Phoenix	Astral Phoenixes	0	3	0	0	0	0	5000	17750	23000	2	0	400	21	28	28	45	60	0	1	2	5	"Fire spell Immunity. Slayer.
Rebirth always. Fire Shield."	IMMUNE_TO_FIRE_SPELLS | DOUBLE_WIDE | FLYING_ARMY | KING_1 | HAS_EXTENDED_ATTACK
Ifrit  Khan	Ifrit Khans	0	0	0	0	0	0	1500	1600	1900	2	0	100	14	18	14	20	25	0	0	5	10	Fire and Air Shield. Fire Immune. Hates Genies.	0
The Luggage	The Luggage	0	0	0	0	2	0	6000	16250	18250	1	0	600	11	25	25	45	75	0	0	2	5	"+1(+2) luck. Ignores defence.
Answers twice. Ignores obstacles."	DOUBLE_WIDE | KING_1
Ghost behemoth stats?
Weird Eye	Weird Eyes	0	0	0	0	0	0	340	300	500	7	0	30	7	12	10	4	8	32	0	12	20	Shoots twice. 	SHOOTING_ARMY
Deadly Wasp	Deadly Wasps	
0

	0	0	0	0	0	340	400	500	8	0	30	14	12	10	4	6	0	0	20	50	Placeholder	Placeholder
Cleric	Clerics	0	0	0	0	0	0	400	588	588	4	0	35	6	12	12	14	20	0	9	10	16	 	 
Ghost Princess	Ghost Princesses	0	0	0	0	0	0	750	150	250	7	0	75	8	11	8	1	2	0	0	16	30	"Attract dead souls."	IS_UNDEAD | IS_GHOST | FLYING_ARMY
Beelzebub	Beelzebub	0	9	0	0	0	0	15000	50000	68750	1	1	1000	20	57	57	64	100	5	5	1	3	 	DOUBLE_WIDE
Elder Pirate	Elder Pirates	0	0	0	0	0	0	400	588	588	4	0	35	6	12	12	14	20	0	0	10	16	 	 
Dark Elf Swordsman	Dark Elf Swordsmen	0	0	0	0	0	0	400	588	588	4	0	35	6	12	12	14	20	0	0	10	16	 	 
Satyr	Satyrs	0	0	0	0	0	0	400	588	588	4	0	35	6	12	12	14	20	0	0	10	16	 	 
Amazon Bowman	Amazon Bowmen	0	0	0	0	0	0	500	500	700	3	0	15	9	12	11	13	17	32	0	8	12	"No walls or range penalty.
No fear. Fire shield."	SHOOTING_ARMY | const_no_wall_penalty
Priestess	Priestesses	0	0	0	0	0	0	650	710	1100	2	0	64	8	15	10	11	14	24	0	6	10	"Protected by Magic Mirror.
No fear. No melee penalty."	SHOOTING_ARMY | const_no_melee_penalty
Chinese Dragon	Chinese Dragons	0	9	0	0	0	0	30000 	56315	78845	1	0	1000	19	50	50	70	80	0	0	1	3	Fear.	DOUBLE_WIDE | FLYING_ARMY | KING_1
azure dragon stats?
Planeswalker	Planeswalkers	0	0	0	0	0	0	600	200	1150	2	0	35	8	15	13	10	13	24	0	5	12	Ranged spell caster.	SHOOTING_ARMY
Elder Ghost	Elder Ghosts	0	0	0	0	0	0	600	150	250	7	0	36	8	11	8	5	7	0	0	16	30	"Attract dead souls."	IS_UNDEAD | IS_GHOST | FLYING_ARMY
Prehistoric Basilisk	Prehistoric Basilisks	0	0	0	0	0	0	750	700	900	4	0	75	7	14	14	8	10	0	0	8	12	"Better Stonifing.
Water Protection. Fearless."	DOUBLE_WIDE
Mummy Servant	Mummy Servants	0	0	0	0	0	0	175	200	200	8	0	25	5	6	6	3	4	0	0	12	25	Curse.	
Centaur Archer	Centaur Archers	0	0	0	0	0	0	250	250	400	14	0	15	8	10	5	4	6	24	0	10	15	Shoots twice.
Merry.	SHOOTING_ARMY | const_two_attacks
Nicolai	Nicolai	0	0	0	0	0	0	750	805	1210	2	0	30	9	17	12	21	21	32	0	5	12	"No melee penalty.
Group spell casters."	SHOOTING_ARMY | const_no_melee_penalty
Sylfaen Dragon
	Sylfaen Dragons	0	0	0	0	0	5	10000	16317	19580	1	0	500	15	20	20	20	30	0	1000	1	3	Offensive spell caster.	DOUBLE_WIDE | FLYING_ARMY | KING_1







