Status: Beta Developing

Mod aimed to rework New_Upgrades/Four_Upgrades without replacing creatures (it would add new via Amethyst)

ready to test and play - Beta developing

Progress
0.0.2 - don't crash at ERA start
0.0.3 - added shooting DEFs
0.1.0 - flags set to proper values instead of placeholder
0.2.0 - fixed flags, first version of script
0.2.1 - fixed Hurricane in script
0.2.2 - replaced amethyst.dll with more recent one, added dummy shoot sound
0.3.0 - added install script for creatures folder, prepared proper Twcport.def and cprsmall.def
0.3.1 - added xp tables
0.4.0 - First Beta
0.4.1 - fixed Stormbird DEF, and installcreatures script
0.4.2 - fixed Dendroid Soldiers Crexp
0.4.3 - added fast battle animation cranim.txt
0.4.4 - added replaces for gnoll and demon upgrades
0.4.5 - added demon resurector to Pit Master, added files for future use
0.4.9 - all creatures packed, need to prepare zcrtrait and cranim
0.5.0 - packed creatures, changed max creature count, prepared beta cranim.txt and placeholder zcrtrait.txt, ready to 7U
0.5.1 - prepared script to match 7 creatures from every town
0.5.2 - filled zcrtrait with proper monster data (AI values still not yet ready), written script to workaround problems with spellcasting
0.5.3 - filled crexpbon.txt entries (creature experience), a few minor fixes
0.5.4 - added temporary creature portraits - much better than old placeholder ones
0.5.5 - fixed dwarf upgrade in script, fixed some creature experience lines
0.5.6 - fixed animation crashes
0.5.7 - first implementaation of Town Guardians
0.5.8 - fixed Spellcaster crashes and Pit Master Ressurection
0.5.9 - reworked magic scripts
0.6.0 - added fix for stupid AI not leaving monsters in garrison